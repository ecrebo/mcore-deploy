# MCORE DEPLOY

Automation of the manual setup scripts detailed in existing documents [MCore Setup Instruction](https://docs.google.com/document/d/1XbtRbQhfDDlm6NvIyXJpPbh7T72oVvi-Iqj0o-f8GqI/edit#heading=h.15wpigeho0tv) and [Mcore Database Setup Instruction](https://docs.google.com/document/d/1lO7xfzWq5Cf6_MMtlcrGR318dNHPpEEoLJ-sKoYE3tQ)

Note: these are designed to simply _automate what is already there_ with minimal changes. _Not_ change/improve the process.

