# Overview

This directory contains Ansible files to configure and install an MCORE database and Wildfly server. It's assumed the OS is already installed and
that SSH access is available to the machines to configure.

The Playbook sets up required OS packages (using yum), creates an install properties file (Wildfly server install only) 
and then uses the existing MCORE setup scripts to install.

## Defined Roles
* db - populates an empty database
* mcore - deploys mcore

## Steps

The steps to use these files are:

- Create a suitable inventory using <retailer>-<environment> as a template, cloning an existing retailer and all files is easiest option

See notes under [Inventories](https://bitbucket.org/ecrebo/mcore-deploy/src/master/ansible/inventories.md) 
- Edit the Ansible variables to provide the required values.
- Use "ansible-vault edit" to update the vault.yml file and input the postgres password used to provision the environment, and an rto password you create (save these in PMP)
- Run the Playbook.

    ```bash
    ## For a full install, db and mcore apps
    ansible-playbook -v -i hosts/<retailer>-<environment>/inventory.ini --ask-vault-pass site.yml
    ## For db setup only
    ansible-playbook -v -i hosts/<retailer>-<environment>/inventory.ini --ask-vault-pass site.yml --limit toolbox
    ## For mcore app setup only (either server)
    ansible-playbook -v -i hosts/<retailer>-<environment>/inventory.ini --ask-vault-pass site.yml --limit mcore2

### Details

Assuming you are working in this directory i.e. `mcore-deploy/ansible` do the following:

Under the `hosts` directory create suitable inventory and variables. See the example. Secrets files can be created and edited using:

`ansible-vault create vault.yml`

`ansible-vault edit vault.yml`

Inventory should contain entries for db and mcore. Example:

```
[db]
toolbox ansible_ssh_host=172.28.128.12 ansible_ssh_port=22 ansible_user=vagrant

[mcore]
mcore1 ansible_ssh_host=172.28.128.4 ansible_ssh_port=22 ansible_user=vagrant
```

this example uses SSH username and publickey login.

The `--ask-vault-pass` is used to prompt for the Ansible vault password. 
An alternative is to use `--vault-password-file` see the Ansible documentation for details.

