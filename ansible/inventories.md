# Using Inventories - some notes
_A bluffer's guide to Ansible_

We are using Ansible here. Ansible has some well defined conventions that make life easier all round. 
But sometimes, these are not necessarily that clear. So here are some notes on how the inventories are intended to be 
used here.

## Why inventories?
An inventory - _and associated `group_vars` structure_ - provides a shrinkwrapped definition of one specific 
environment. This means the parameters for deploying to,say, QA, Staging, or even Production can be checked into version 
control rather thanrelying on the manual entry of environment-specific details. This is a _Good Thing_.

## What's the structure? 
Inventories can be stored anywhere - but in this project, there is a `hosts` directory acting as a convenient root:
```
    hosts---target_env---inventory.ini
          |            |
          |             ---group_vars---all---vars.yml
          |                           |     |
          |                           |      -vault.yml
          |                           |
          |                            - db---vars.yml
          |                           |     |
          |                           |      -vault.yml
          |                           |
          |                            -mcore---vars.yml
          |                                   |
          |                                    -vault.yml
          |
           -target_env---group_vars-etc etc etc
```

`groupvars/all` has variables used by all roles

`group_var/<role_name>` defines variables for the specific roles

[All standard Ansible stuff](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#id10).

## Using secrets
We suggest that the [Ansible recommendation for storing secrets](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html#variables-and-vaults) is 
adopted to allow specific passwords to be checked in encrypted. Specifically this is:

* Declare the actual secret value as a variable in the appropriate `vault.yml` encrypted [Ansible Vault](https://docs.ansible.com/ansible/2.4/vault.html) 
file using the convention `vault_<name>`
* Assign the secret value to a variable named `<name>` in the appropriate `vars.yml` file using Jinja syntax.

So for example, `vault.yml` might define:
```yaml
vault_my_supersecret_password: mysecretvalue 
``` 

Assign it using 
```yaml
my_supersecret_password: "{{ vault_my_supersecret_password }}"
```

Ansible takes care of the rest. Obviously, don't lose (or check in!) the main Vault password...