---

- name: Fetch the CORE app DB password from local machine directory
  local_action: "shell grep app_{{ retailer_name }}_mcore ~/{{ retailer_name }}-{{ mode }}/db_passwords_coredb.txt |tail -1|cut -f3"
  register: core_dbpassword_out

- name: Set app CORE app DB password as an Ansible fact
  set_fact:
    coredb_password: '{{ core_dbpassword_out.stdout | trim}}'

- name: Set app OPS app DB password as an Ansible fact
  set_fact:
    opsdb_password: '{{ coredb_password }}'

    #- name: Install postgres96 repository
    #yum:
    # name:
    #  - https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-6-x86_64/pgdg-ami201503-96-9.6-2.noarch.rpm
    #become: true
    #tags: postgres

- name: Enable epel-release repository
  command: yum-config-manager --enable epel
  become: true
  when: ansible_distribution == "Amazon"

- name: Install packages required to run the database connection check
  yum:
    name:
      - java-1.8.0-openjdk-devel
      - perl
      - postgresql96
      - postgresql96-devel
  become: true

- name: Update Java version from 1.7 to 1.8
  alternatives: name=java path=/usr/lib/jvm/jre-1.8.0-openjdk.x86_64/bin/java
  become: yes

- name: Add Postgres client to PATH
  shell: echo 'export PATH=$PATH:/usr/pgsql-9.6/bin/' > /etc/profile.d/postgresql.sh
  args:
    creates: /etc/profile.d/postgresql.sh
  become: true

- name: Check MCORE database user connection
  shell: "export PGPASSWORD='{{ coredb_password }}' && psql -h {{ coredb }} -c 'SELECT 1' -U app_{{ retailer_name }}_mcore {{ retailer_name }}_core{{db_suffix}}"
  args:
    executable: /bin/bash

# Below step is needed otherwise ant process overrides the placeholder value passed by the jinja template
- name: Clear PGPASSWORD to prevent it being picked up by ant
  shell: "unset PGPASSWORD"
  args:
    executable: /bin/bash

# libselinux-python is needed by the Ansible copy module.
- name: Install tools
  become: yes
  become_method: sudo
  yum: 
    name:
      - libselinux-python
      - perl
      - git
      - https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.2.1/wkhtmltox-0.12.2.1_linux-centos6-amd64.rpm
      - xorg-x11-fonts-75dpi
      - dejavu-sans-fonts.noarch
    state: latest
    
- name: Download and extract Ant
  become: yes
  become_method: sudo
  unarchive:
    src: http://apache.org/dist/ant/binaries/apache-ant-1.10.5-bin.tar.gz
    dest: /usr/share
    remote_src: yes
    creates: /usr/share/ant

- name: Rename Ant dir
  become: yes
  become_method: sudo
  command: mv /usr/share/apache-ant-1.10.5 /usr/share/ant
  args:
    creates: /usr/share/ant

- name: Link ant to usr bin
  become: yes
  become_method: sudo
  file:
    src: /usr/share/ant/bin/ant
    dest: /usr/bin/ant
    state: link    

- name: Edit .bash_profile to add ANT_HOME
  blockinfile:
    path: "~/.bash_profile"
    block: |
      export ANT_HOME=/usr/share/ant

- name: Git clone MCORE repo
  git:
    repo: "https://{{ git_user | urlencode }}:{{ git_password | urlencode }}@bitbucket.org/ecrebo/mcore.git"
    dest: "~/mcore"
    version: "{{ mcore_version }}"
    force: yes
  tags:
    - repo

- name: Copy checkout-mcore script
  copy:
    src: files/checkout_mcore.sh
    dest: "~/"
    mode: 0755
  tags:
    - repos

- name: Checkout the correct mcore version
  shell: ./checkout_mcore.sh --mcore-version {{ mcore_version }}
  args:
    chdir: ~/
  tags:
    - repos

- name: Get file stat for mcore-infinispan-v3.xml
  stat: path=~/mcore/mco-bom/mcore-infinispan-v3.xml
  register: infinispanfile

- name: Copy mcore-infinispan-v3.xml to mcore-infinispan.xml if v3 exists
  copy:
    src: ~/mcore/mco-bom/mcore-infinispan-v3.xml
    dest: ~/mcore/mco-bom/mcore-infinispan.xml
    remote_src: yes
  when: infinispanfile.stat.exists

- name: Add user and group for wildfly
  become: yes
  become_method: sudo
  user:
    name: wildfly

- name: Define build.properties file
  template:
    src: build-properties.jinja
    dest: "~/mcore/mco-releng/build.properties"
    mode: 0666
                        
- name: Download the plugin EAR file and put into home directory
  get_url:
    url: "{{ plugin_url }}"
    url_username: "{{ atf_user }}"
    url_password: "{{atf_password }}"
    dest: "~/{{ plugin }}"

- name: Run ant to build the wildfly instance
  shell: ant
  args:
    chdir: "~/mcore/mco-releng"

- name: Move new wildfly directory to srv
  command: mv /home/{{ ansible_ssh_user }}/mcore/mco-releng/target/srv/wildfly-{{ retailer_name }} /srv/
  become: yes

- name: Download the plugin EAR file and put into deployments directory
  get_url:
    url: "{{ plugin_url }}"
    url_username: "{{ atf_user }}"
    url_password: "{{atf_password }}"
    dest: "/srv/wildfly-{{ retailer_name }}/standalone/deployments/{{ plugin }}"

    # Commented out as not required for initial deployment but needed later.
    #- name: REPLACE ME - Copy mcoupload war file from local machine into deployments directory
    #copy:
    #src: "~/mcore-install/mcoupload-4.2.2.war"
    #dest: "/srv/wildfly-{{ retailer_name }}/standalone/deployments/"
    #mode: 0664
    #become: yes

- name: Update permissions on new wildfly instance
  file:
    path: /srv/wildfly-{{ retailer_name }}
    state: directory
    owner: wildfly
    group: wildfly
    recurse: yes
  become: yes

- name: Update permission on standalone-full.xml file
  file:
    path: "/srv/wildfly-{{ retailer_name }}/standalone/configuration/standalone-full.xml"
    mode: 0600
  become: yes

- name: Put the CORE app DB password in the standalone-full.xml file
  replace:
    path: "/srv/wildfly-{{ retailer_name }}/standalone/configuration/standalone-full.xml"
    regexp: 'REPLACE_MCORE_APP_PW'
    replace: '{{ coredb_password }}'
  become: yes

  # Commented out as custom ami already includes!
  #- name: Add the sysctl.conf block for MCORE
  #blockinfile:
  #  path: "/etc/sysctl.conf"
  #  marker: "<!-- {mark} ANSIBLE MANAGED BLOCK -->"
  #  block: |
  #    Required for JGroups (WildFly)
  #    net.core.rmem_max=25000000
  #    net.core.wmem_max=20000000
  #become: yes

- name: Dynamically update the sysctl settings for this server uptime session
  command: sysctl -w net.core.rmem_max=25000000; sudo sysctl -w net.core.wmem_max=20000000
  become: yes

- name: Copy security limits file for wildfly user
  copy:
    src: /home/{{ ansible_ssh_user }}/mcore/mco-releng/target/etc/security/limits.d/90-nproc.conf
    dest: /etc/security/limits.d/
    remote_src: yes
  become: yes

- name: Copy init file for wildfly service
  copy:
    src: /home/{{ ansible_ssh_user }}/mcore/mco-releng/target/etc/init.d/wildfly-{{ retailer_name }}
    dest: /etc/init.d/
    remote_src: yes
    mode: 0755
  become: yes

- name: Copy defaults file for wildfly service
  copy:
    src: /home/{{ ansible_ssh_user }}/mcore/mco-releng/target/etc/default/wildfly-{{ retailer_name }}.conf
    dest: /etc/default/
    remote_src: yes
    mode: 0600
    owner: wildfly
    group: wildfly
  become: yes

- name: Add the wildfly service
  command: chkconfig --add wildfly-{{ retailer_name }}
  become: yes

- name: Enable the wildfly service
  service: 
    name: wildfly-{{ retailer_name }} 
    enabled: yes
  become: yes

- name: Start wildfly service
  service: 
    name: wildfly-{{ retailer_name }} 
    state: started
  become: yes

- name: Configuring Wildfly management user
  command: /srv/wildfly-{{ retailer_name }}/bin/add-user.sh --user {{ wildfly_username }} --password {{ wildfly_password }} --realm ManagementRealm --confirm-warning
  become: yes
