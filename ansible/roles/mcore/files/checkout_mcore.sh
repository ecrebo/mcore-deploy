#!/bin/bash

help="usage: $(basename $0)
$(perl -n -e '/"(--[^"]+)"( +\| )"(-[^"])".*#(.*)/ && print "  $1$2$3) $4\n"' $0)"

MCORE_VERSION=
MCORE_HOME=~/mcore

while [ $# -gt 0 ]; do
  case "$1" in
"--mcore-home"       | "-H") shift; MCORE_HOME=$1; shift;; #mcore home direcotry (git clone)
"--mcore-version"    | "-v") shift; MCORE_VERSION=$1; shift;; #mcore version, optional
  *                          ) echo -e "$help"; exit 1;;
  esac
done

#Checkout MCORE version
if [ "${MCORE_VERSION}" == "" ]; then echo "[OK] mcore version is not specified."; else
  if [ ! -d ${MCORE_HOME} ]; then echo "[ERROR] mcore not found."; exit 1; else
    (cd ${MCORE_HOME};
    tag_branch_name="$(git name-rev --tags --name-only $(git rev-parse HEAD) | sed 's/\^0//')";
    if [ "${tag_branch_name}" == "undefined" ]; then
      tag_branch_name="$(git branch | awk '/\*/{print $2}')";
    fi
    echo "You have: ${tag_branch_name}";
    if [ "${tag_branch_name}" == "${MCORE_VERSION}" ]; then
      echo "mcore ${MCORE_VERSION} have already checked out";
    else
      git checkout ${MCORE_VERSION};
    fi)
  fi
fi
