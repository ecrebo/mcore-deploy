#!/bin/bash
help="echo -ne \"usage: $(basename $0) $OPTIONS\n$(perl -ne '/"(--[^"]+)"( +\| +)"(-[^"]+)".*#(.*)/ && print "  $1$2$3) $4\\n"' $0)\""

DEFAULT_PORT=5432
DEFAULT_ADMIN_USER=postgres

host=
port=5432
port_core=
port_oc=
port_ops=
retailer=
suffix=
db_core=
db_oc=
db_ops=
mcore_user=
admin_user=
quiet=false

while [ $# -gt 0 ]; do
  case "$1" in
  "--host"       |  "-h")  shift;       host=$1; shift;; #hostname of core/ops databases
  "--host-core"  | "-hc") shift;  host_core=$1; shift;; #hostname of core database (default=\$host)
  "--host-oc"    | "-hoc") shift;  host_core=$1; shift;; #hostname of core database (default=\$host)
  "--host-ops"   | "-ho") shift;   host_ops=$1; shift;; #hostname of ops database (default=\$host)
  "--port"       |  "-P")  shift;       port=$1; shift;; #database port number of core/ops databases (default=${DEFAULT_PORT})
  "--port-core"  | "-Pc") shift;  port_core=$1; shift;; #database port number of core database (default=${DEFAULT_PORT})
  "--port-oc"    | "-Poc") shift;  port_core=$1; shift;; #database port number of core database (default=${DEFAULT_PORT})
  "--port-ops"   | "-Po") shift;   port_ops=$1; shift;; #database port number of ops database (default=${DEFAULT_PORT})
  "--db-core"    |  "-c")  shift;    db_core=$1; shift;; #core database name (retailer + "_core" + suffix), skip if - is specified
  "--db-oc"      |  "-oc")  shift;    db_core=$1; shift;; #core database name (retailer + "_core" + suffix), skip if - is specified
  "--db-ops"     |  "-o")  shift;     db_ops=$1; shift;; #ops database name (retailer + "_ops" + suffix), skip if - is specified
  "--retailer"   |  "-r")  shift;   retailer=$1; shift;; #retailer name (used for compose database name)
  "--suffix"     |  "-s")  shift;     suffix=$1; shift;; #database name suffix (specify _test for lab)
  "--mcore-user" |  "-u")  shift; mcore_user=$1; shift;; #database user for MCORE
  "--admin-user" |  "-a")  shift; admin_user=$1; shift;; #database admin user (need ~/.pgpass, default=${DEFAULT_USER})
  "--quiet"      |  "-q")            quiet=true; shift;; #set quiet mode                
                       *)  eval "$help"; exit 1;;
  esac
done

if [ "$port"       == "" ]; then port=${DEFAULT_PORT}; fi
if [ "$host_core"  == "" ]; then host_core=$host; fi
if [ "$port_core"  == "" ]; then port_core=$port; fi
if [ "$host_oc"    == "" ]; then host_oc=$host; fi
if [ "$port_oc"    == "" ]; then port_oc=$port; fi
if [ "$host_ops"   == "" ]; then host_ops=$host; fi
if [ "$port_ops"   == "" ]; then port_ops=$port; fi
if [ "$admin_user" == "" ]; then admin_user=${DEFAULT_ADMIN_USER}; fi
if [ "$retailer"  != "" ]; then 
  lowercase_retailer=$(echo ${retailer} | awk '{print tolower($0)}')
  if [ "$db_core" == "" ]; then db_core=${lowercase_retailer}_core${suffix}; fi
  if [ "$db_oc"   == "" ]; then db_oc=${lowercase_retailer}_oc${suffix}; fi
  if [ "$db_ops"  == "" ]; then db_ops=${lowercase_retailer}_ops${suffix}; fi
fi

if [ "$mcore_user" == "" ]; then echo "ERROR: mcore_user is null."; eval "$help"; exit 1; fi
if [ "$admin_user" == "" ]; then echo "ERROR: admin_user is null."; eval "$help"; exit 1; fi

if [ "$quiet" == "false" ] ; then
  echo "======== parameters ========"
  echo "host_core  = $host_core"
  echo "port_core  = $port_core"
  echo "db_core    = $db_core"
  echo "host_oc    = $host_oc"
  echo "port_oc    = $port_oc"
  echo "db_oc      = $db_oc"
  echo "host_ops   = $host_ops"
  echo "port_ops   = $port_ops"
  echo "db_ops     = $db_ops"
  echo "mcore_user = $mcore_user"
  echo "admin_user = $admin_user"
  echo "======= confirmation ======="
  sure=; read -p "Do you want to progress with above parameters? [y|Quit] " sure;
  if [ "$sure" != "y" ] && [ "$sure" != "Y" ]; then echo quit; exit 1; fi
fi

# Table lookup to get all table names
#tables_core="`psql -U $admin_user -h $host_core $db_core -tc "select tablename from pg_tables where schemaname = 'public';"`"
#tables_oc="`psql -U $admin_user -h $host_oc $db_oc -tc "select tablename from pg_tables where schemaname = 'public';"`"
#tables_ops="`psql -U $admin_user -h $host_ops $db_ops -tc "select tablename from pg_tables where schemaname = 'public';"`"

tables_core="serve_counters"

tables_ops="
jgroupsping
mcore5_app_requests
mcore5_app_responses
mcore5_mgmt_requests
mcore5_mgmt_responses
mcore5_transactions
mcore5_transaction_parse_errors
mcore5_transaction_aux
mcore5_app_api_logs_view
mcore5_transactions_view
z_mcore_pos_transaction_commits
z_mcore_pos_transactions
z_mcore_trace_logs
"

if [ "$host_core" != "" ] && [ "$port_core" != "" ] && [ "$db_core" != "" ] && [ "$db_core" != "-"  ] && [ "$tables_core" != "" ]; then
  for table in ${tables_core}; do
    echo $table;
    psql -U $admin_user -h $host_core -p $port_core $db_core -c "ALTER TABLE ${table} OWNER to ${mcore_user}"
  done
else
  echo "skipped core database.";
fi

if [ "$host_oc" != "" ] && [ "$port_oc" != "" ] && [ "$db_oc" != "" ] && [ "$db_oc" != "-"  ] && [ "$tables_oc" != "" ]; then
  for table in ${tables_oc}; do
    echo $table;
    psql -U $admin_user -h $host_oc -p $port_oc $db_oc -c "ALTER TABLE ${table} OWNER to ${mcore_user}"
  done
else
  echo "skipped oc database.";
fi

if [ "$host_ops" != "" ] && [ "$port_ops" != "" ] && [ "$db_ops" != "" ] && [ "$db_ops" != "-"  ] && [ "$tables_ops" != "" ]; then
  for table in ${tables_ops}; do
    echo $table;
    psql -U $admin_user -h $host_ops -p $port_ops $db_ops -c "ALTER TABLE ${table} OWNER to ${mcore_user}"
  done
else
  echo "skipped ops database.";
fi

